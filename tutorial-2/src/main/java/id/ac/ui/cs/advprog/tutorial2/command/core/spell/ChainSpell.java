package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ChainSpell implements Spell {
    protected ArrayList<Spell> listOfSpells;

    public ChainSpell(ArrayList<Spell> listOfSpells) {
        this.listOfSpells = listOfSpells;
    }

    @Override
    public void cast() {
        for (Spell spell : listOfSpells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = listOfSpells.size() - 1; i >= 0; i--) {
            Spell spell = listOfSpells.get(i);
            spell.undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
