package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            return rawAge*2000;
        } else if (rawAge <50) {
            return rawAge*2250;
        } else {
            return rawAge*5000;
        }
    }

    @Override
    public String powerClassifier(int birthYear) {
        /**
         * Apabila kekuatannya antara 0 - 20000, tambahkan teks 'C class' disamping power di html
         * Apabila kekuatannya antara 20000 - 100000 tambahkan teks 'B class' .
         * Apabila kekuatannya lebih dari 100000, tambahkan teks 'A class'
         * */
        int age = countPowerPotensialFromBirthYear(birthYear);
        if (age >= 0 && age <= 20000) {
            return "C class";
        } else if (age > 20000 && age <= 100000) {
            return "B class";
        } else if (age > 100000){
            return "A class";
        } else {
            return "";
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
