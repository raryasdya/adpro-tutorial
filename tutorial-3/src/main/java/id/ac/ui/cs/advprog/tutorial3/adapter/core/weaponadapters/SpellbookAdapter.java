package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean wasLargeSpell;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.wasLargeSpell = false;
    }

    @Override
    public String normalAttack() {
        wasLargeSpell = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (wasLargeSpell) {
            return "Unable to use large spell twice in a row!";
        } else {
            wasLargeSpell = true;
            return spellbook.largeSpell();
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }
}
