package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FullMoonPike implements Weapon {

    private String holderName;

    public FullMoonPike(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return "Normal Full Moon Pike Attack!";
    }

    @Override
    public String chargedAttack() {
        return "Charged Full Moon Pike Attack!";
    }

    @Override
    public String getName() {
        return "Full Moon Pike";
    }

    @Override
    public String getHolderName() {
        return holderName;
    }
}
