package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class StaffOfHoumo implements Weapon {

    private String holderName;

    public StaffOfHoumo(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return "Normal Staff of Houmo Attack!";
    }

    @Override
    public String chargedAttack() {
        return "Charged Staff of Houmo Attack!";
    }

    @Override
    public String getName() {
        return "Staff of Houmo";
    }

    @Override
    public String getHolderName() {
        return holderName;
    }
}
