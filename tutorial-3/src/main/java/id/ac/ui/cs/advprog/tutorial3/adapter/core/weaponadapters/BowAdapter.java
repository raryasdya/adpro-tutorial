package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean aimShot;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.aimShot = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(aimShot);
    }

    @Override
    public String chargedAttack() {
        this.aimShot = !this.aimShot;
        return this.aimShot ? "Entered aim shot mode" : "Leaving aim shot mode";
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
