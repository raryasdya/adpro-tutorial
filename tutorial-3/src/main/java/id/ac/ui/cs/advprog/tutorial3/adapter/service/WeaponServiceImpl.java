package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeaponServiceImpl implements WeaponService {

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private LogRepository logRepository;

    @Override
    public List<Weapon> findAll() {
        List<Weapon> listOfWeapons = weaponRepository.findAll();

        for (Bow bow : bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                listOfWeapons.add(new BowAdapter(bow));
            }
        }

        for (Spellbook spellbook : spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null) {
                listOfWeapons.add(new SpellbookAdapter(spellbook));
            }
        }

        return listOfWeapons;
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = this.getWeapon(weaponName);
        if (weapon != null) {
            weaponRepository.save(weapon);
            logRepository.addLog(
                String.format(
                    "%s attacked with %s (%s): %s!",
                    weapon.getHolderName(),
                    weapon.getName(),
                    attackType == 0 ? "Normal Attack" : "Charged Attack",
                    attackType == 0
                        ? weapon.normalAttack()
                        : weapon.chargedAttack()
                )
            );
        }
    }

    public Weapon getWeapon(String weaponName) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if (weapon != null) {
            return weapon;
        }

        Bow bow = bowRepository.findByAlias(weaponName);
        if (bow != null) {
            return (new BowAdapter(bow));
        }

        Spellbook spellbook = spellbookRepository.findByAlias(weaponName);
        if (spellbook != null) {
            return (new SpellbookAdapter(spellbook));
        }

        return null;
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
