package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation {

    public CaesarTransformation(){
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){

        String text = spell.getText();
        Codex codex = spell.getCodex();

        int shift = encode ? 1 : -1;
        int codexSize = codex.getCharSize();
        char[] res = new char[text.length()];
        for(int i = 0; i < text.length(); i++){
            char oldChar = text.charAt(i);
            int charIdx = codex.getIndex(oldChar);

            int newCharIdx = charIdx + shift;
            newCharIdx = newCharIdx < 0 ? codexSize + newCharIdx : newCharIdx % codexSize;
            res[i] = codex.getChar(newCharIdx);
        }

        return new Spell(new String(res), codex);
    }
}
