package id.ac.ui.cs.advprog.tutorial3.facade.core.codex;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

public class RunicCodexTest {

    @Test
    public void testRunicCodexIsSingleton() {
        assertSame(RunicCodex.getInstance(), RunicCodex.getInstance());
    }
}
