package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SeawardPrideTest {

    private Class<?> seawardPrideClass;
    private SeawardPride seawardPride;

    @BeforeEach
    public void setUp() throws Exception {
        seawardPrideClass =
            Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.SeawardPride"
            );
        seawardPride = new SeawardPride("Someone");
    }

    @Test
    public void testSeawardPrideIsConcreteClass() {
        assertFalse(Modifier.isAbstract(seawardPrideClass.getModifiers()));
    }

    @Test
    public void testSeawardPrideIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(
            seawardPrideClass.getInterfaces()
        );

        assertTrue(
            interfaces
                .stream()
                .anyMatch(
                    type ->
                        type
                            .getTypeName()
                            .equals(
                                "id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon"
                            )
                )
        );
    }

    @Test
    public void testSeawardPrideOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = seawardPrideClass.getDeclaredMethod(
            "normalAttack"
        );

        assertEquals(
            "java.lang.String",
            normalAttack.getGenericReturnType().getTypeName()
        );
        assertEquals(0, normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testSeawardPrideOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = seawardPrideClass.getDeclaredMethod(
            "chargedAttack"
        );

        assertEquals(
            "java.lang.String",
            chargedAttack.getGenericReturnType().getTypeName()
        );
        assertEquals(0, chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testSeawardPrideOverrideGetNameMethod() throws Exception {
        Method getName = seawardPrideClass.getDeclaredMethod("getName");

        assertEquals(
            "java.lang.String",
            getName.getGenericReturnType().getTypeName()
        );
        assertEquals(0, getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testSeawardPrideOverrideGetHolderMethod() throws Exception {
        Method getHolderName = seawardPrideClass.getDeclaredMethod(
            "getHolderName"
        );

        assertEquals(
            "java.lang.String",
            getHolderName.getGenericReturnType().getTypeName()
        );
        assertEquals(0, getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    public void testSeawardPrideAttack() throws Exception {
        assertEquals(
            "Normal Seaward Pride Attack!",
            seawardPride.normalAttack()
        );
        assertEquals(
            "Charged Seaward Pride Attack!",
            seawardPride.chargedAttack()
        );
    }

    @Test
    public void testSeawardPrideGetName() throws Exception {
        assertEquals("Seaward Pride", seawardPride.getName());
    }

    @Test
    public void testSeawardPrideGetHolderName() throws Exception {
        assertEquals("Someone", seawardPride.getHolderName());
    }
}
