package id.ac.ui.cs.advprog.tutorial3.facade.core.codex;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

public class AlphaCodexTest {

    @Test
    public void testAlphaCodexIsSingleton() {
        assertSame(AlphaCodex.getInstance(), AlphaCodex.getInstance());
    }
}
